import 'package:flutter/material.dart';
//import 'package:gitlab_mate/pages/CreateIssue.dart';
//import 'package:gitlab_mate/pages/projectDescription.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'IssueDescription.dart';

class Commit {
  String id;
  String shortId;
  String createdAt;
  List<String> parentIds;
  String title;
  String message;
  String authorName;
  String authorEmail;
  String authoredDate;
  String committerName;
  String committerEmail;
  String committedDate;
  String webUrl;

  Commit(
      {this.id,
      this.shortId,
      this.createdAt,
      this.parentIds,
      this.title,
      this.message,
      this.authorName,
      this.authorEmail,
      this.authoredDate,
      this.committerName,
      this.committerEmail,
      this.committedDate,
      this.webUrl});

  Commit.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    shortId = json['short_id'];
    createdAt = json['created_at'];
    parentIds = json['parent_ids'].cast<String>();
    title = json['title'];
    message = json['message'];
    authorName = json['author_name'];
    authorEmail = json['author_email'];
    authoredDate = json['authored_date'];
    committerName = json['committer_name'];
    committerEmail = json['committer_email'];
    committedDate = json['committed_date'];
    webUrl = json['web_url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['short_id'] = this.shortId;
    data['created_at'] = this.createdAt;
    data['parent_ids'] = this.parentIds;
    data['title'] = this.title;
    data['message'] = this.message;
    data['author_name'] = this.authorName;
    data['author_email'] = this.authorEmail;
    data['authored_date'] = this.authoredDate;
    data['committer_name'] = this.committerName;
    data['committer_email'] = this.committerEmail;
    data['committed_date'] = this.committedDate;
    data['web_url'] = this.webUrl;
    return data;
  }
}

class ProjectCommitList extends StatefulWidget {
  final String token;
  final String projectId;
  final String title;

  ProjectCommitList({this.token, this.projectId, this.title});
  @override
  _ProjectCommitListState createState() => _ProjectCommitListState();
}

class _ProjectCommitListState extends State<ProjectCommitList> {
  List<Commit> _projectCommitList = new List<Commit>();
  Future<List<Commit>> _fetchCommitList(String token, String projectId) async {
    final jobsListAPIUrl =
        'https://gitlab.com/api/v4/projects/$projectId/repository/commits?private_token=$token';
    final response = await http.get(jobsListAPIUrl);
    if (response.statusCode == 200) {
      List<dynamic> values = new List<dynamic>();
      setState(() {
        values = json.decode(response.body);
      });

      if (values.length > 0) {
        for (int i = 0; i < values.length; i++) {
          if (values[i] != null) {
            Map<String, dynamic> map = values[i];
            _projectCommitList.add(Commit.fromJson(map));
          }
        }
      }
      return _projectCommitList;
    } else {
      throw Exception('Failed to load post');
    }
  }

  @override
  void initState() {
    super.initState();
    this._fetchCommitList(widget.token, widget.projectId);
    _projectCommitList.sort();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          SizedBox(
            height: 30.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  SizedBox(
                    width: 18.0,
                  ),
                  Container(
                    child: Text(
                      widget.title,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 28.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
              FlatButton(
                textColor: Colors.white,
                onPressed: () {
                  showSearch(
                    context: context,
                    delegate:
                        ProjectIssueSearch(_projectCommitList, widget.token),
                  );
                },
                child: Icon(
                  Icons.search,
                  color: Colors.orange,
                ),
                shape: CircleBorder(
                  side: BorderSide(
                    color: Colors.transparent,
                  ),
                ),
              ),
            ],
          ),
          // Card(
          //   shape: RoundedRectangleBorder(
          //     borderRadius: BorderRadius.circular(15.0),
          //   ),
          //   margin: EdgeInsets.all(10.0),
          //   // child: Column(
          //   //   children: <Widget>[
          //   //     // ListTile(
          //   //     //     // leading: Icon(Icons.add),
          //   //     //     // title: Text(
          //   //     //     //   "Create new commit!",
          //   //     //     //   style: TextStyle(
          //   //     //     //     color: Colors.black,
          //   //     //     //     fontWeight: FontWeight.w400,
          //   //     //     //     fontSize: 14.0,
          //   //     //     //   ),
          //   //     //     // ),
          //   //     //     // onTap: () {
          //   //     //     //   Navigator.push(
          //   //     //     //     context,
          //   //     //     //     MaterialPageRoute(
          //   //     //     //       builder: (context) => CreateIssue(
          //   //     //     //         token: widget.token,
          //   //     //     //         title: widget.title,
          //   //     //     //         projectId: widget.projectId,
          //   //     //     //       ),
          //   //     //     //     ),
          //   //     //     //   );
          //   //     //     // },
          //   //     //     ),
          //   //   ],
          //   // ),
          // ),
          createListView(_projectCommitList),
        ],
      ),
    );
  }

  Widget createListView(var listVariable) {
    return Flexible(
      child: new ListView.builder(
        itemCount: listVariable.length,
        itemBuilder: (BuildContext context, int index) {
          return Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            margin: EdgeInsets.all(15.0),
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 10.0,
                ),
                ListTile(
                  title: Text(listVariable[index].title),
                  subtitle: RichText(
                    text: TextSpan(
                      text: '\n',
                      style: new TextStyle(
                        fontSize: 5.0,
                        color: Colors.grey,
                      ),
                      children: <TextSpan>[
                        TextSpan(
                          text: "\n",
                          style: TextStyle(fontSize: 0.0, color: Colors.orange),
                        ),
                        TextSpan(
                          text: listVariable[index].message + "\n",
                          style: TextStyle(fontSize: 12.5, color: Colors.grey),
                        ),
                      ],
                    ),
                  ),
                  trailing: Text(
                    listVariable[index].shortId,
                    style: TextStyle(
                      fontSize: 12.5,
                      color: Colors.blue,
                    ),
                  ),
                  onTap: () {
                    // Navigator.push(
                    //   context,
                    //   MaterialPageRoute(
                    //     builder: (context) => IssueDescription(
                    //       token: widget.token,
                    //       issueId: _projectCommitList[index].issueId,
                    //       title: _projectIssueList[index].issueTitle,
                    //       //issueId: _issueList[index].issueId,
                    //     ),
                    //   ),
                    // );
                  },
                ),
                SizedBox(
                  height: 10.0,
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}

class ProjectIssueSearch extends SearchDelegate<Commit> {
  final List<Commit> projectIssueList;
  static Commit result;
  final String token;
  ProjectIssueSearch(this.projectIssueList, this.token);
  @override
  List<Widget> buildActions(BuildContext context) {
    // Todo: implement buildActions

    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = "";
        },
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    // Todo: implement buildLeading
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return null;
  }

  //Add the search term to the searchBloc.
  //The Bloc will then handle the searching and add the results to the searchResults stream.
  //This is the equivalent of submitting the search term to whatever search service you are using

  @override
  Widget buildSuggestions(BuildContext context) {
    // Todo: implement buildSuggestions

    var list = query.isEmpty
        ? projectIssueList
        : projectIssueList
            .where((p) => p.title.toLowerCase().startsWith(query.toLowerCase()))
            .toList();
    return list.isEmpty
        ? Card(
            child: ListTile(
              title: Center(
                child: Text("No commits found..."),
              ),
            ),
          )
        : ListView.builder(
            itemCount: list.length,
            itemBuilder: (context, index) {
              final showList = list[index];
              return ListTile(
                onTap: () {
                  result = showList;
                  query = showList.title;
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => IssueDescription(
                        token: token,
                        issueId: showList.shortId,
                        //projectId: result.projectId,
                      ),
                    ),
                  );
                  //showResults(context);
                },
                title: Text(showList.title),
                trailing: RichText(
                  text: TextSpan(
                    children: <TextSpan>[
                      TextSpan(
                        text: showList.shortId,
                        style: TextStyle(
                          fontSize: 12.0,
                          color: Colors.blue,
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          );
  }
}
