import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'dart:convert';
import 'dart:async';

Future<User> fetchUser(String token, String username) async {
  final response =
      await http.get('https://gitlab.com/api/v4/users?username=$username');
  if (response.statusCode == 200) {
    final jsonresponse = json.decode(response.body);
    print(response.body);
    return User.fromJson(jsonresponse[0]);
  } else {
    throw Exception('Failed to load Issue');
  }
}

class User {
  User({
    this.id,
    this.name,
    this.username,
    this.state,
    this.avatarUrl,
    this.webUrl,
  });

  int id;
  String name;
  String username;
  String state;
  String avatarUrl;
  String webUrl;

  factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["id"],
        name: json["name"],
        username: json["username"],
        state: json["state"],
        avatarUrl: json["avatar_url"],
        webUrl: json["web_url"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "username": username,
        "state": state,
        "avatar_url": avatarUrl,
        "web_url": webUrl,
      };
}

class UserProfile extends StatefulWidget {
  UserProfile(
      {this.token, this.userAvatar, this.userName, this.name, this.userId});

  //final String title;
  final String token;
  final String userAvatar;
  final String name;
  final String userName;
  final String userId;

  @override
  _UserProfileState createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  Future<User> futureUser;
  @override
  void initState() {
    super.initState();
    futureUser = fetchUser(widget.token, widget.userName);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: 20.0,
              ),
              Hero(
                tag: "user-avatar",
                child: Container(
                  margin: EdgeInsets.symmetric(vertical: 30),
                  padding: EdgeInsets.all(6),
                  height: 200,
                  width: 200,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.grey.withOpacity(.5),
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(100.0)),
                    child: CachedNetworkImage(
                      width: 100.0,
                      height: 100.0,
                      fit: BoxFit.cover,
                      imageUrl: widget.userAvatar,
                      placeholder: (context, url) =>
                          new CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation(Colors.yellow[700]),
                      ),
                      errorWidget: (context, url, error) =>
                          new Icon(Icons.error),
                    ),
                  ),
                ),
              ),
              ListTile(
                title: Center(
                  child: Text(
                    widget.name,
                    style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w400,
                      fontSize: 27.0,
                    ),
                  ),
                ),
                subtitle: Center(
                  child: Text(
                    "(" + widget.userId + ")",
                    style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w300,
                      fontSize: 17.0,
                    ),
                  ),
                ),
              ),
              ListTile(
                title: Center(
                  child: Text(
                    widget.userName,
                    style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w200,
                      fontSize: 20.0,
                    ),
                  ),
                ),
              ),
              Container(
                child: FutureBuilder<User>(
                  future: futureUser,
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        margin: EdgeInsets.all(15.0),
                        child: Column(
                          children: <Widget>[
                            ListTile(
                              leading: Icon(
                                Icons.people_outline,
                                size: 18.0,
                                color: Colors.blue,
                              ),
                              title: Text(
                                snapshot.data.name,
                                style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w300,
                                  fontSize: 14.0,
                                ),
                              ),
                            ),
                            ListTile(
                              leading: Icon(
                                Icons.link,
                                size: 18.0,
                                color: Colors.red,
                              ),
                              title: Text(
                                snapshot.data.webUrl,
                                style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w300,
                                  fontSize: 14.0,
                                ),
                              ),
                            ),
                            ListTile(
                              leading: FaIcon(
                                FontAwesomeIcons.checkCircle,
                                size: 18.0,
                                color: Colors.green,
                              ),
                              title: Text(
                                snapshot.data.state,
                                style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w300,
                                  fontSize: 14.0,
                                ),
                              ),
                            ),
                            SizedBox(height: 20),
                            ListTile(
                              title: Row(
                                children: <Widget>[
                                  Expanded(
                                    child: RaisedButton(
                                      onPressed: () {
                                        follow(snapshot.data.id, widget.token);
                                      },
                                      child: Row(
                                        children: [
                                          Center(
                                            child: Text("   Follow"),
                                          ),
                                        ],
                                      ),
                                      color: Colors.black38,
                                      textColor: Colors.white,
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            new BorderRadius.circular(100.0),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10.0,
                                  ),
                                  Expanded(
                                    child: RaisedButton(
                                      onPressed: () {
                                        unFollow(
                                            snapshot.data.id, widget.token);
                                      },
                                      child: Row(
                                        children: [
                                          Text(
                                            "   Unfollow",
                                          ),
                                        ],
                                      ),
                                      color: Colors.black38,
                                      textColor: Colors.white,
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            new BorderRadius.circular(100.0),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 15.0,
                            ),
                          ],
                        ),
                      );
                    } else if (snapshot.hasError) {
                      return Text("${snapshot.error}");
                    }
                    return Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      margin: EdgeInsets.all(15.0),
                      child: Column(
                        children: <Widget>[
                          ListTile(
                            title: Center(
                              child: CircularProgressIndicator(),
                            ),
                            onTap: () {},
                          ),
                        ],
                      ),
                    );
                  },
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
            ],
          ),
        ),
      ),
    );
  }

  void unFollow(int id, String token) async {
    final response = await http.post(
        'https://gitlab.com/api/v4//users/$id/follow?private_token=$token');
    print(response.body);
    if (response.statusCode == 201) {
      //return Project.fromJson(jsonDecode(response.body));
      print("followed");
    } else {
      throw Exception('Failed to foloow');
    }
  }

  void follow(int id, String token) async {
    final response = await http.post(
        'https://gitlab.com/api/v4//users/$id/unfollow?private_token=$token');
    if (response.statusCode == 201) {
      //return Project.fromJson(jsonDecode(response.body));
      print("unfollowed");
    } else {
      throw Exception('Failed to un follow');
    }
  }
}
