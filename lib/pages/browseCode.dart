import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:gitlab_mate/pages/viewFile.dart';
import 'package:http/http.dart' as http;
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'dart:async';

List<RepositoryFile> repositoryFileFromJson(String str) =>
    List<RepositoryFile>.from(
        json.decode(str).map((x) => RepositoryFile.fromJson(x)));

String repositoryFileToJson(List<RepositoryFile> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class RepositoryFile {
  RepositoryFile({
    this.id,
    this.name,
    this.type,
    this.path,
    this.mode,
  });

  String id;
  String name;
  Type type;
  String path;
  String mode;

  factory RepositoryFile.fromJson(Map<String, dynamic> json) => RepositoryFile(
        id: json["id"],
        name: json["name"],
        type: typeValues.map[json["type"]],
        path: json["path"],
        mode: json["mode"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "type": typeValues.reverse[type],
        "path": path,
        "mode": mode,
      };
}

enum Type { TREE, BLOB }

final typeValues = EnumValues({"blob": Type.BLOB, "tree": Type.TREE});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}

class BrowseFiles extends StatefulWidget {
  BrowseFiles({this.title, this.token, this.projectId, this.path});

  final String title;
  final String token;
  final String projectId;
  final String path;

  @override
  _BrowseFilesState createState() => _BrowseFilesState();
}

class _BrowseFilesState extends State<BrowseFiles> {
  List<RepositoryFile> _fileList = new List<RepositoryFile>();
  Future<List<RepositoryFile>> _futureFiles;

  Future<List<RepositoryFile>> fetchFiles(
      String token, String projectId, String path) async {
    final response = await http.get(
        'https://gitlab.com/api/v4/projects/$projectId/repository/tree/?path=$path&private_token=$token');

    if (response.statusCode == 200) {
      List<dynamic> values = new List<dynamic>();
      values = json.decode(response.body);
      if (values.length > 0) {
        for (int i = 0; i < values.length; i++) {
          if (values[i] != null) {
            Map<String, dynamic> map = values[i];
            _fileList.add(RepositoryFile.fromJson(map));
          }
        }
      }
      return _fileList;
    } else {
      throw Exception('Failed to load post');
    }
  }

  @override
  void initState() {
    super.initState();
    _futureFiles = fetchFiles(widget.token, widget.projectId, widget.path);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          SizedBox(
            height: 30.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  SizedBox(
                    width: 18.0,
                  ),
                  Container(
                    child: Text(
                      widget.title,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 28.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
              FlatButton(
                textColor: Colors.white,
                onPressed: () {
                  showSearch(
                    context: context,
                    delegate: ProjectSearch(_fileList, widget.token, false),
                  );
                },
                child: Icon(
                  Icons.search,
                  color: Colors.orange,
                ),
                shape: CircleBorder(
                  side: BorderSide(
                    color: Colors.transparent,
                  ),
                ),
              ),
            ],
          ),
          FutureBuilder<List<RepositoryFile>>(
            future: _futureFiles,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                if (snapshot.data.length > 0) {
                  return Expanded(
                    child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      margin: EdgeInsets.all(5.0),
                      child: ListView.builder(
                        itemCount: snapshot.data.length,
                        itemBuilder: (_, int index) {
                          return Container(
                            child: Column(
                              children: <Widget>[
                                ListTile(
                                  leading:
                                      snapshot.data[index].type == Type.BLOB
                                          ? FaIcon(
                                              FontAwesomeIcons.file,
                                              color: Colors.blueGrey,
                                            )
                                          : FaIcon(
                                              FontAwesomeIcons.solidFolder,
                                              color: Colors.blue,
                                            ),
                                  title: Text(snapshot.data[index].name),
                                  trailing:
                                      snapshot.data[index].type != Type.BLOB
                                          ? Icon(
                                              Icons.arrow_forward_ios,
                                              size: 15,
                                              color: Colors.grey[400],
                                            )
                                          : Text(""),
                                  onTap: () {
                                    if (snapshot.data[index].type !=
                                        Type.BLOB) {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => new BrowseFiles(
                                            token: widget.token,
                                            title: "> " +
                                                snapshot.data[index].name,
                                            projectId: widget.projectId,
                                            path: snapshot.data[index].path,
                                          ),
                                        ),
                                      );
                                    } else {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => new ViewCode(
                                            token: widget.token,
                                            title: "> " +
                                                snapshot.data[index].name,
                                            name: snapshot.data[index].name,
                                            projectId: widget.projectId,
                                            id: snapshot.data[index].id,
                                          ),
                                        ),
                                      );
                                    }
                                  },
                                ),
                                Divider(
                                  color: Colors.grey[300],
                                  endIndent: 0.0,
                                  indent: 0.0,
                                )
                              ],
                            ),
                          );
                        },
                      ),
                    ),
                  );
                } else {
                  return Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    margin: EdgeInsets.all(15.0),
                    child: Column(
                      children: <Widget>[
                        ListTile(
                          title: Center(
                            child: Text(
                              "No Projects Found.",
                              style: TextStyle(
                                fontWeight: FontWeight.w200,
                                fontSize: 14.0,
                              ),
                            ),
                          ),
                          onTap: () {},
                        ),
                      ],
                    ),
                  );
                }
              } else if (snapshot.hasError) {
                return Text("${snapshot.error}");
              }
              return Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                margin: EdgeInsets.all(15.0),
                child: Column(
                  children: <Widget>[
                    ListTile(
                      title: Center(
                        child: CircularProgressIndicator(),
                      ),
                      onTap: () {},
                    ),
                  ],
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}

class ProjectSearch extends SearchDelegate<RepositoryFile> {
  final List<RepositoryFile> projectList;
  static RepositoryFile result;
  final String token;
  bool flag;
  ProjectSearch(this.projectList, this.token, this.flag);
  @override
  List<Widget> buildActions(BuildContext context) {
    // Todo: implement buildActions

    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = "";
        },
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    // Todo: implement buildLeading
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return null;
  }

  //Add the search term to the searchBloc.
  //The Bloc will then handle the searching and add the results to the searchResults stream.
  //This is the equivalent of submitting the search term to whatever search service you are using

  @override
  Widget buildSuggestions(BuildContext context) {
    // Todo: implement buildSuggestions

    var list = query.isEmpty
        ? projectList
        : projectList
            .where((p) => p.name.toLowerCase().startsWith(query.toLowerCase()))
            .toList();
    return list.isEmpty
        ? Card(
            child: ListTile(
              title: Center(
                child: Text("No files found..."),
              ),
            ),
          )
        : ListView.builder(
            itemCount: list.length,
            itemBuilder: (context, index) {
              final showList = list[index];
              return ListTile(
                onTap: () {
                  result = showList;
                  query = showList.name;
                  //showResults(context);
                },
                leading: showList.type == Type.BLOB
                    ? FaIcon(
                        FontAwesomeIcons.file,
                        color: Colors.blueGrey,
                        size: 20.0,
                      )
                    : FaIcon(
                        FontAwesomeIcons.solidFolder,
                        color: Colors.blue,
                        size: 20.0,
                      ),
                title: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      showList.name,
                      style: TextStyle(
                        fontSize: 14.0,
                      ),
                    ),
                    SizedBox(
                      height: 5.0,
                    ),
                    Divider(),
                  ],
                ),
              );
            },
          );
  }
}
