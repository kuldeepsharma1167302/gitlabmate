import 'package:flutter/material.dart';
import 'package:gitlab_mate/pages/home.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:gitlab_mate/services/storage.dart' as storage;
import 'dart:async';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'GitLab Mate',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.orange,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: SplashScreen(),
    );
  }
}

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SplashScreenState();
  }
}

class SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    _loadSavedToken();
    startTimer();
  }

  _loadSavedToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      storage.savedToken = (prefs.getString('savedToken') ?? null);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Hero(
              tag: "gitlab-mate-icon",
              child: Image(
                height: 180.0,
                width: 170.0,
                image: AssetImage('assets/icons/gitlab-mate-yellow-icon.png'),
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            CircularProgressIndicator(
              backgroundColor: Colors.yellow[700],
              valueColor: AlwaysStoppedAnimation(Colors.orange),
            ),
          ],
        ),
      ),
    );
  }

  void startTimer() {
    Timer(
      Duration(seconds: 3),
      () {
        navigateUser(); //It will redirect  after 3 seconds
      },
    );
  }

  void navigateUser() async {
    var status = (storage.savedToken) ?? null;
    if (status != null) {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (BuildContext context) => HomePage(
            token: storage.savedToken,
          ),
        ),
      );
    } else {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (BuildContext context) => RootPage(),
        ),
      );
    }
  }
}

class RootPage extends StatefulWidget {
  RootPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _RootPageState createState() => _RootPageState();
}

class _RootPageState extends State<RootPage> {
  TextEditingController personalTokenController = TextEditingController();

  _setSavedToken(String token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setString('savedToken', token);
      storage.savedToken = (prefs.getString('savedToken') ?? token);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Hero(
              tag: "gitlab-mate-icon",
              child: Image(
                height: 180.0,
                width: 170.0,
                image: AssetImage('assets/icons/gitlab-mate-yellow-icon.png'),
              ),
            ),
            RichText(
              text: TextSpan(
                text: 'Welcome to ',
                style: new TextStyle(
                  fontSize: 16.0,
                  color: Colors.grey,
                ),
                children: <TextSpan>[
                  TextSpan(
                    text: 'GitLab ',
                    style: TextStyle(color: Colors.orange),
                  ),
                  TextSpan(
                    text: 'Mate',
                    style: TextStyle(color: Colors.yellow[700]),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 40.0,
            ),
            Container(
              margin: EdgeInsets.only(left: 20.0, right: 20.0),
              child: TextFormField(
                controller: personalTokenController,
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Invalid Access Token';
                  }
                  return null;
                },
                decoration: new InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderSide:
                        BorderSide(color: Colors.yellow[700], width: 2.0),
                    borderRadius: BorderRadius.circular(50.0),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.orange, width: 2.0),
                    borderRadius: BorderRadius.circular(50.0),
                  ),
                  hintText: 'Personal Access Token',
                ),
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            RaisedButton(
              elevation: 0.0,
              shape: CircleBorder(
                side: BorderSide(color: Colors.white),
              ),
              onPressed: () {
                _setSavedToken(personalTokenController.text);
                if (storage.savedToken != null) {
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (context) => HomePage(token: storage.savedToken),
                    ),
                  );
                } else {
                  Center(
                    child: CircularProgressIndicator(
                      backgroundColor: Colors.yellow[700],
                      valueColor: AlwaysStoppedAnimation(Colors.orange),
                    ),
                  );
                }
              },
              color: Colors.white,
              child: Icon(
                Icons.arrow_forward,
                color: Colors.yellow[600],
                size: 40.0,
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
          ],
        ),
      ),
    );
  }
}
